package com.example.services;

import com.example.Repository.UserRepository;
import com.example.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public  Users updateUser(Users newUser) {
        return userRepository.save(newUser);
    }

    public boolean register(Users user){
        if(checkUserCreated(user)){
            userRepository.save(user);
            return true;
        }else return false;
    }

    public boolean checkUserCreated(Users user){
        Users oldUser = userRepository.findUsersByEmail(user.getEmail());
        return oldUser == null;
    }

    public Users login(Users user){
        return userRepository.findUsersByEmailAndPassword(user.getEmail(), user.getPassword());
    }

    public boolean changePin(String email , int pin, int newPin){
        Users userData = userRepository.findUsersByEmailAndPin(email, pin);
        if(userData == null) return false;
        try {
            userData.setPin(newPin);
            userRepository.save(userData);
            return true;
        }catch (Exception e){
            return false;
        }

    }


    public Users isUser(Users user){
        return userRepository.findUsersByEmail(user.getEmail()) ;
    }

    public Users isUserWithPin(Users user, int pin) {
        return userRepository.findUsersByEmailAndPin(user.getEmail(), pin);
    }

    public boolean transfers(Users dataUser, Users dataTransferee, double amount) {
        dataUser.setBalance(dataUser.getBalance() - amount);
        dataTransferee.setBalance(dataTransferee.getBalance() + amount);
        try {
            userRepository.save(dataUser);
            userRepository.save(dataTransferee);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public List<Users> findAll() {
        return userRepository.findAll();
    }

    public boolean deleteUser(Users user) {
        try{
            userRepository.delete(user);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
