package com.example.services;

import com.example.Repository.TransactionRepository;
import com.example.Repository.UserRepository;
import com.example.model.Transaction;
import com.example.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service
public class TransactionServices {
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    UserRepository userRepository;

    public boolean setBalance(Users banker,Users user, double amount) {

        Users dataUser = userRepository.findUsersByEmailAndPassword(user.getEmail(), user.getPassword());
        Users dataBanker = userRepository.findUsersByEmailAndPassword(banker.getEmail(), banker.getPassword());
        if(isBanker(dataBanker)) {
            if (dataUser != null) {
                double newBalance = dataUser.getBalance() + amount;
                dataUser.setBalance(newBalance);
                userRepository.save(dataUser);

                Transaction transaction = new Transaction();
                transaction.setContent("Nạp tiền vào tài khoản từ ngân hàng");
                transaction.setIdUser(dataUser.getId());
                transaction.setType("recharge");
                transaction.setTransfereeId(dataBanker.getId());
                transaction.setCreateAt(new Date());
                transactionRepository.save(transaction);
                return true;
            }
        }
        return false;
    }

    public Transaction saveTransaction(Transaction transaction){
        return transactionRepository.save(transaction);
    }
    public boolean isBanker(Users user){
        return user.getRoles().toUpperCase(Locale.ROOT).equals("BANK");
    }

    public boolean withdrawal(Users user, double amount) {

        user.setBalance(user.getBalance() - amount);
        Transaction transaction = new Transaction();
        transaction.setCreateAt(new Date());
        transaction.setContent("Rút tiền từ tài khoản " + user.getId() + " số tiền " + amount);
        transaction.setType("withdrawal");
        transaction.setIdUser(user.getId());
        try{
            userRepository.save(user);
            transactionRepository.save(transaction);
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    public boolean transfers(Users dataUser, Users dataTransferee, double amount, String content) {
        Transaction transaction = new Transaction();
        transaction.setIdUser(dataUser.getId());
        transaction.setContent(content);
        transaction.setType("transfers");
        transaction.setTransfereeId(dataTransferee.getId());
        transaction.setCreateAt(new Date());
        try {
            transactionRepository.save(transaction);
            return true;
        }catch (Exception e){
            return  false;
        }

    }

    public List<Transaction> getAllByIdUser(String idUser) {
        return transactionRepository.findAllByIdUser(idUser);
    }
}
