package com.example.Controllers;

import com.example.model.Users;
import com.example.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    UserService userService;

    @GetMapping("/user")
    public List<Users> getAllUser(){
        return userService.findAll();
    }

    @PutMapping("/user")
    public Users updateUser(@RequestBody Users newUser){
        return userService.updateUser(newUser);
    }

    @DeleteMapping
    public boolean deleteUser(@RequestBody Users user){
        return userService.deleteUser(user);
    }
}
