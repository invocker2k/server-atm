package com.example.Controllers;


import com.example.Repository.UserRepository;
import com.example.model.Users;
import com.example.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody Users user){

            if(userService.register(user))
            return new ResponseEntity<>(
                    "success",
                    HttpStatus.OK);

            return new ResponseEntity<>(
                    "User has exist",
                    HttpStatus.CREATED);

    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody Users user){
            Users userData =  userService.login(user);
        if(userData != null)
            return new ResponseEntity<>(
                    userData,
                    HttpStatus.OK);

        return new ResponseEntity<>(
                "Sai mật khẩu hoặc tài khoản không tồn tại",
                HttpStatus.UNAUTHORIZED);
    }

    @PostMapping(value = "/changePin")
    public ResponseEntity<String> changePin(@RequestBody Map<String, Object> payload){

        if(userService.changePin((String) payload.get("email"),  Integer.parseInt((String) payload.get("pin")), Integer.parseInt((String) payload.get("newPin"))))
            return new ResponseEntity<String>(
                    "Đổi mã phin thành công",
                    HttpStatus.OK);
       else
           return new ResponseEntity<String>(
                    "Thay đổi mã pin thất bại, nhập lại mã pin",
                    HttpStatus.OK);
    }
}