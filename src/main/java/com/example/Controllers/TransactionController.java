package com.example.Controllers;

import com.example.model.Transaction;
import com.example.model.Users;
import com.example.services.TransactionServices;
import com.example.services.UserService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {

    @Autowired
    TransactionServices transactionServices;
    @Autowired
    UserService userService;

    @PostMapping("/recharge")
    public ResponseEntity<String> recharge(@RequestBody Map<String, Object> payload){
        Map<String, Object> user = (Map<String, Object>) payload.get("user");
        Map<String, Object> banker = (Map<String, Object>) payload.get("banker");

        boolean isRecharge = transactionServices.setBalance(
                new Users((String)banker.get("email"), (String) banker.get("password")),
                new Users((String)user.get("email"), (String) user.get("password")),
                Double.parseDouble((String)payload.get("amount")));
        if(isRecharge)
        return new ResponseEntity<String>(
                "Nạp tiền thành công vào tài khoản",
                HttpStatus.OK
        );

        return new ResponseEntity<String>(
                "Thử lại sau",
                HttpStatus.FOUND
        );

    }

    @PostMapping("/withdrawal")
    public ResponseEntity<String> withdrawal(@RequestBody Map <String, Object> payload) {
        Map<String, Object> user = (Map<String, Object>) payload.get("user");
        Users dataUser = userService.isUserWithPin(
                new Users((String)user.get("email"), (String) user.get("password")),
                Integer.parseInt((String)user.get("pin"))
        );
        if (dataUser != null) {
            boolean withdrawal = transactionServices.withdrawal(dataUser, Double.parseDouble( (String) payload.get("amount")));
            if (withdrawal) {
                return new ResponseEntity<String>(
                        "Bạn đẫ rút tiền khỏi tài khoản, số tiền: " + payload.get("amount") + "$",
                        HttpStatus.OK
                );
            }
        }
            return new ResponseEntity<String>(
                    "Giao dịch thất bại",
                    HttpStatus.FOUND
            );
    }

    @PostMapping("/transfers")
    public ResponseEntity<String> transfers(@RequestBody Map<String, Object> payload){
        double amount = Double.parseDouble((String) payload.get("amount"));
        String content = (String) payload.get("content");

        Map<String, Object> user = (Map<String, Object>) payload.get("user");
        Map<String, Object> transferee = (Map<String, Object>) payload.get("transferee");
        Users dataUser = userService.isUserWithPin(
                new Users((String)user.get("email"), (String) user.get("password")),
                Integer.parseInt((String)user.get("pin"))
        );
        Users dataTransferee = new Users();
        dataTransferee.setEmail((String)transferee.get("email"));
        dataTransferee = userService.isUser(dataTransferee);
        boolean isUserTransfers =  userService.transfers(dataUser, dataTransferee, amount);
        boolean isTransactionsTransfers = transactionServices.transfers(dataUser, dataTransferee, amount, content);
        if(isTransactionsTransfers && isUserTransfers){
            return new ResponseEntity<String>(
                    "Giao Thành công",
                    HttpStatus.OK
            );
        }
        return new ResponseEntity<String>(
                "Giao dịch thất bại",
                HttpStatus.FOUND
        );
    }

    @PostMapping("/balance-inquiry")
    public List<Transaction> balanceInquiry(@RequestBody Users user){
        System.out.println(user.toString());
        Users dataUser = userService.isUserWithPin(user, user.getPin());

        return transactionServices.getAllByIdUser(dataUser.getId());

    }

}
