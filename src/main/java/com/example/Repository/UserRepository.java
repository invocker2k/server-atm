package com.example.Repository;


import com.example.model.Users;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<Users, String> {
    Users findUsersByEmail(String email);
    Users findUsersByEmailAndPassword(String email, String password);
    Users findUsersByEmailAndPin(String email, int pin);

}

