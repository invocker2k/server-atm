package com.example.Repository;

import com.example.model.Transaction;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TransactionRepository  extends MongoRepository<Transaction, String> {
    List<Transaction> findAllByIdUser(String id);
}
